import TweenLite from 'gsap';

require('gsap/ScrollToPlugin');

class Anhors {
  constructor() {
    this.header = document.querySelector('.dev-header');
    this.headerBtn = document.querySelector('.dev-header__menu-btn');
    this.anhors = [].slice.call(document.querySelectorAll('.core-anhors'));

    this.anhors.forEach((item) => {
      item.addEventListener('click', (event) => {
        event.preventDefault();
        const block = document.querySelector(item.getAttribute('href'));
        const scroll = window.pageYOffset;
        const point = (block.getBoundingClientRect().top + scroll);

        if (this.header && this.headerBtn) {
          this.header.classList.remove('dev-header--open');
          this.headerBtn.classList.remove('dev-header__menu-btn--open');
          document.body.style.overflow = '';
        }

        TweenLite.to(window, 2, { scrollTo: point });
      });
    });
  }
}
export default Anhors;
