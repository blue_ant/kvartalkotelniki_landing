import GMaps from 'gmaps';

class Map {
  constructor(data) {
    this.data = data;
    this.map = document.querySelector('.dev-map__map');

    if (this.map) {
      this.initMap();
    }
  }

  showMarkers() {
    this.data.forEach((item) => {
      this.apiMap.addMarker(item);
    });
  }

  initMap() {
    this.apiMap = new GMaps({
      div: this.map,
      mapTypeControl: false,
      scrollwheel: false,
      streetViewControl: false,
      zoom: 12,
      lat: 55.656427,
      lng: 37.828006,
      styles: [],
    });
    this.showMarkers();
    this.apiMap.fitZoom();
  }
}

export default Map;
