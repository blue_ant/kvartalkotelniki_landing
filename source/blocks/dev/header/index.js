class Header {
  constructor() {
    this.header = document.querySelector('.dev-header');
    this.headerBtn = document.querySelector('.dev-header__menu-btn');

    if (this.headerBtn) {
      this.headerBtn.addEventListener('click', (event) => {
        event.preventDefault();
        this.header.classList.toggle('dev-header--open');
        this.headerBtn.classList.toggle('dev-header__menu-btn--open');

        if (this.headerBtn.classList.contains('dev-header__menu-btn--open')) {
          document.body.style.overflow = 'hidden';
        } else {
          document.body.style.overflow = '';
        }
      });
    }
  }
}

export default Header;
