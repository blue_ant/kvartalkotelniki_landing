class Flats {
  constructor() {
    this.flats = [].slice.call(document.querySelectorAll('.dev-flats__img-wrap'));

    this.flats.forEach((flat) => {
      if (flat.classList.contains('dev-flats__img-wrap--slider')) {
        this.constructor.render(flat);
      }
    });
  }

  static render(flat) {
    const clipper = flat.querySelector('.dev-flats__clipper');
    const img = flat.querySelector('.dev-flats__new-img');
    const slider = flat.querySelector('.dev-flats__img-slider');
    let posPersetn = 0.5;

    slider.addEventListener('touchmove', (e) => {
      const widthFlat = flat.clientWidth;
      const padding = widthFlat * 0.04;
      const sliderCoords = flat.getBoundingClientRect().left;
      let newLeft = e.changedTouches[0].pageX - sliderCoords;
      const rightEdge = flat.offsetWidth - padding;

      if (newLeft < padding) {
        newLeft = padding;
      } else if (newLeft > rightEdge) {
        newLeft = rightEdge;
      }

      const pos = (newLeft / widthFlat).toFixed(5);
      posPersetn = pos * 100;
      clipper.style.width = `${posPersetn}%`;
      slider.style.left = `${posPersetn}%`;
      img.style.width = `${(100 / pos) + 0.1}%`;
    });

    slider.addEventListener('mousedown', (e) => {
      const thumbCoords = slider.getBoundingClientRect().left;
      const shiftX = e.pageX - thumbCoords;
      const sliderCoords = flat.getBoundingClientRect().left;

      document.onmousemove = (event) => {
        const widthFlat = flat.clientWidth;
        const padding = widthFlat * 0.025;
        let newLeft = event.pageX - shiftX - sliderCoords;
        const rightEdge = flat.offsetWidth - padding;

        if (newLeft < padding) {
          newLeft = padding;
        } else if (newLeft > rightEdge) {
          newLeft = rightEdge;
        }

        const pos = (newLeft / widthFlat).toFixed(3);
        posPersetn = pos * 100;
        clipper.style.width = `${posPersetn}%`;
        slider.style.left = `${posPersetn}%`;
        img.style.width = `${100 / pos}%`;
      };

      document.onmouseup = () => {
        document.onmousemove = null;
        document.onmouseup = null;
      };

      return false;
    });

    slider.ondragstart = () => false;
  }
}

export default Flats;
