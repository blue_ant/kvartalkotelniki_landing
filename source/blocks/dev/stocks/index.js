import Swiper from 'swiper';

class Stocks {
  constructor() {
    if (window.innerWidth < 768) {
      new Swiper('.dev-stocks__items', {
        slidesPerView: 1,
        spaceBetween: 20,
        spead: 700,
        loop: false,
      });
    }
  }
}

export default Stocks;
