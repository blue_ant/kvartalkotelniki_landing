import 'svgxuse';

import Anhors from './blocks/core/anhors';
import Flats from './blocks/dev/flats';
import Header from './blocks/dev/header';
import Stocks from './blocks/dev/stocks';
import Map from './blocks/dev/map';

require('./autoload.scss');

new Anhors();
new Flats();
new Header();
new Stocks();

const markers = [
  {
    title: 'ЖК «Новые Котельники»',
    icon: {
      url: '/s-images/map/logo_mark.png',
      scaledSize: new google.maps.Size(50, 51),
    },
    details: {
      type: 1100,
    },
    lat: 55.638325,
    lng: 37.857704,
    infoWindow: {
      content: 'ЖК «Новые Котельники»',
      pixelOffset: new google.maps.Size(-5, 0),
    },
  },
  {
    title: 'метро «Котельники»',
    icon: {
      url: '/s-images/map/metro.png',
      scaledSize: new google.maps.Size(45, 45),
    },
    details: {
      type: 15,
    },
    lat: 55.673720,
    lng: 37.859038,
    infoWindow: {
      content: 'метро «Котельники»',
      pixelOffset: new google.maps.Size(-5, 0),
    },
  },
  {
    title: 'метро «Алма-Атинская»',
    icon: {
      url: '/s-images/map/metro.png',
      scaledSize: new google.maps.Size(45, 45),
    },
    details: {
      type: 1102,
    },
    lat: 55.632815,
    lng: 37.766049,
    infoWindow: {
      content: 'метро «Алма-Атинская»',
      pixelOffset: new google.maps.Size(-5, 0),
    },
  },
];

new Map(markers);
